

rm -f xdemineur
gcc -o xdemineur -g -O2 -fno-strict-aliasing       demineur.o main.o util.o xdemineur.o -lXpm -lXext -lX11      
rm -f xdemineur._man
if test -z "true" ; then \
   cd `dirname xdemineur` && \
   ln -s `basename xdemineur.man` `basename xdemineur._man`; \
else \
             cpp -undef -traditional  -D__apploaddir__=/etc/X11/app-defaults -D__filemansuffix__=5x -D__osfilemansuffix__=5 -D__libmansuffix__=3x -D__oslibmansuffix__=3 -D__mansuffix__=1x -D__osmansuffix__=1 -D__syscallmansuffix__=2x -D__ossysmansuffix__=2 -D__gamemansuffix__=6x -D__osgamemansuffix__=6 -D__miscmansuffix__=7x -D__osmiscmansuffix__=7 -D__admmansuffix__=8x -D__osadmmansuffix__=8 -D__miscmansuffix__=7x -D__osmiscmansuffix__=7 -D__drivermansuffix__=4x -D__osdrivermansuffix__=4 -D__adminmansuffix__=8 -D__projectroot__=/usr -D__xconfigfile__=xorg.conf -D__xconfigdir__=/usr/lib/X11 -D__xlogfile__=Xorg -D__xservername__=Xorg -D__appmansuffix__=1x -D__xorgversion__="\"`echo 7 7 0 | sed -e 's/ /./g' -e 's/^/Version\\\ /'`\" \"X Version 11\"" -D__vendorversion__="`echo 7 7 0 | sed -e 's/ /./g' -e 's/^/Version\\\ /'` X.Org"  \
     < xdemineur.man | sed -e '/^#  *[0-9][0-9]*  *.*$/d'                       -e '/^#line  *[0-9][0-9]*  *.*$/d'                      -e '/^[         ]*XCOMM$/s/XCOMM/#/'                    -e '/^[         ]*XCOMM[^a-zA-Z0-9_]/s/XCOMM/#/'                        -e '/^[         ]*XHASH/s/XHASH/#/'                    -e '/\@\@$/s/\@\@$/\\/' >xdemineur._man; \
fi




